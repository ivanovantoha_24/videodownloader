//
//  Video.swift
//  JSON
//
//  Created by Anton Ivanov on 09.04.16.
//  Copyright © 2016 TonyCo. All rights reserved.
//

import Foundation

class Video  {
    
    var title: String = ""
    var references: String = ""
    
    init(title:String, references: String){
        self.title = title
        self.references = references
    }
 
}