//
//  ButtonCell.swift
//  CellButtons
//
//  Created by Jure Zove on 20/09/15.
//  Copyright © 2015 Candy Code. All rights reserved.
//

import UIKit

protocol ButtonCellDelegate {
    func cellTapped(cell: ButtonCell, tap: Int, sender: UIButton)->Downloader
    func actionTap(cell: ButtonCell, tap: Int, sender: UIButton, dd: Downloader)
}

class ButtonCell: UITableViewCell {
    
    var buttonDelegate: ButtonCellDelegate?
    @IBOutlet weak var rowLabel: UILabel!
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    
    @IBOutlet weak var procent: UILabel!
    
    var dd = Downloader()
    var button1 = UIButton()
    
    @IBAction func removeAction(sender: UIButton) {
        self.dd.pause()
        self.dd.cancel()
        self.button1.setTitle("download", forState: .Normal)
        self.dd.tap.qInt = 0
        self.dd.tableViewCell.procent.text = ""
        self.dd.tableViewCell.statusLabel.text = "Status:"

        
        
    }
    
    @IBAction func buttonTap(sender: UIButton) {
        button1  = sender
        if self.dd.tap.qInt == 0 {
                  if let delegate = buttonDelegate {
                    self.dd = delegate.cellTapped(self, tap: self.dd.tap.qInt, sender: sender)
            }
        }else
            if(dd.tap.qInt == 2 || dd.tap.qInt == 1){
                if let delegate = buttonDelegate {
                    delegate.actionTap(self, tap: self.dd.tap.qInt, sender: sender, dd: dd)
                }
                
        }
    }

}
