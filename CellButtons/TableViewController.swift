//
//  TableViewController.swift
//  CellButtons
//
//  Created by Jure Zove on 20/09/15.
//  Copyright © 2015 Candy Code. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController, ButtonCellDelegate {
    
    var videosArray = QArray()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.leftBarButtonItem = self.editButtonItem()
        
        let addButton = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: #selector(self.first10))
        self.navigationItem.rightBarButtonItem = addButton
        
        let pars = Parser(tableViewController: self)
        pars.parseJSON1()
        self.videosArray = pars.NamesArray
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.videosArray.videos.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ButtonCell", forIndexPath: indexPath) as! ButtonCell

        if self.videosArray.videos.count != 0{
            cell.rowLabel.text  = self.videosArray.videos[indexPath.row].title
        }
        if cell.buttonDelegate == nil {
            cell.buttonDelegate = self
        }

        return cell
    }
    
    // MARK: - ButtonCellDelegate
    
    func cellTapped(cell: ButtonCell, tap: Int, sender: UIButton) -> Downloader{
        
        let dd = Downloader()
            sender.setTitle("pause", forState: .Normal)
            dd.tap.qInt = 1
        let jj = tableView.indexPathForCell(cell)?.row
        dd.startDownload(cell, ref: self.videosArray.videos[jj!].references, incr: jj!)
  
        return dd
    }

    func actionTap(cell: ButtonCell, tap: Int, sender: UIButton, dd: Downloader){
        if dd.tap.qInt == 1 {
            sender.setTitle("resume", forState: .Normal)
            dd.pause()
            dd.tap.qInt = 2
        }else
            if dd.tap.qInt == 2 {
                sender.setTitle("pause", forState: .Normal)
                dd.resume()
                dd.tap.qInt = 1
        }
    }
    

    
    // MARK: - Extracted method
    


    func first10(){
        for i in 0...2 {
        let indexPath = NSIndexPath(forRow: i, inSection: 0)
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! ButtonCell
        cell.button1.setTitle("pause", forState: .Normal)
        cell.dd = self.cellTapped(cell, tap: cell.dd.tap.qInt, sender: cell.button1)   
        }
    }

}
