//
//  Download.swift
//  JSON
//
//  Created by Anton Ivanov on 09.04.16.
//  Copyright © 2016 TonyCo. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class Parser {
    
    var NumberOfRows = 0
    var NamesArray = QArray()
    var jsonForDownload =  JSON("")
    var tableViewController = UITableViewController()
    
    
    init(tableViewController: UITableViewController){
        self.tableViewController = tableViewController
    }
    
    
    func parseJSON1(){
        Alamofire.request(.GET, "https://devimages.apple.com.edgekey.net/wwdc-services/ftzj8e4h/6rsxhod7fvdtnjnmgsun/videos.json").validate().responseJSON { response in
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                    self.jsonForDownload = json
                    self.download()
                    self.NumberOfRows = json["sessions"].count
                    for i in 0...self.NumberOfRows - 1 {
                        
                        let title = json["sessions"][i]["title"].string as String!
                        let referenses = json["sessions"][i]["download_sd"].string as String!
                        if (title != nil && referenses != nil){
                        let element = Video(title: title, references: referenses)
                        self.NamesArray.videos.append(element)
                        }
                    }
                    self.tableViewController.tableView.reloadData()
                }
            case .Failure(let error):
                print(error)
            }
        }
    }
    
    
    func download(){
        let stringURL = self.jsonForDownload["sessions"][0]["download_sd"].string as String!
        
        //  First you need to create your video url
        
        if let videoUrl = NSURL(string: "http://devstreaming.apple.com/videos/wwdc/2015/216isrjt4ku9w4/216/216_sd_layout_and_animation_techniques_for_watchkit.mp4") {
            
            // then lets create your document folder url
            let documentsDirectoryURL =  NSFileManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
            
            // lets create your destination file url
            let destinationUrl = documentsDirectoryURL.URLByAppendingPathComponent(videoUrl.lastPathComponent ?? "video.mp4")
            print(destinationUrl)
            
            // to check if it exists before downloading it
            if NSFileManager().fileExistsAtPath(destinationUrl.path!) {
                print("The file already exists at path")
                
                // if the file doesn't exist
            } else {
                
                // you can use NSURLSession.sharedSession to download the data asynchronously
                NSURLSession.sharedSession().downloadTaskWithURL(videoUrl, completionHandler: { (location, response, error) -> Void in
                    guard let location = location where error == nil else { return }
                    do {
                        // after downloading your file you need to move it to your destination url
                        try NSFileManager().moveItemAtURL(location, toURL: destinationUrl)
                        print("File moved to documents folder")
                    } catch let error as NSError {
                        print(error.localizedDescription)
                    }
                }).resume()
            }
        }
        print(stringURL)
        
    }
    
}
