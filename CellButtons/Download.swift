//
//  Download.swift
//  JSON
//
//  Created by Anton Ivanov on 09.04.16.
//  Copyright © 2016 TonyCo. All rights reserved.
//

import Foundation
import UIKit
class Downloader: NSObject, NSURLSessionDownloadDelegate, UIDocumentInteractionControllerDelegate {
    
    
    var downloadTask: NSURLSessionDownloadTask!
    var backgroundSession: NSURLSession!
    
    var progressView: UIProgressView!
    var tableViewCell: ButtonCell!
    var tap = QInt()

    
    
    func startDownload(tableViewCell: ButtonCell!, ref: String, incr: Int) {
        self.tableViewCell = tableViewCell
        let path = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        print(path)
        
        let backgroundSessionConfiguration = NSURLSessionConfiguration.backgroundSessionConfigurationWithIdentifier(String(incr))
        
        backgroundSession = NSURLSession(configuration: backgroundSessionConfiguration, delegate: self, delegateQueue: NSOperationQueue.mainQueue())

        let url = NSURL(string: ref)!
        downloadTask = backgroundSession.downloadTaskWithURL(url)
        downloadTask.resume()
    }

    func pause() {
        if downloadTask != nil{
            downloadTask.suspend()
        }
        
    }
    func resume() {
        if downloadTask != nil{
            downloadTask.resume()
        }
        
    }
    func cancel() {
        if downloadTask != nil{
            downloadTask.cancel()
            self.tableViewCell.progressBar.setProgress(0.0, animated: true)
        }
    }

    // 1
    @objc func URLSession(session: NSURLSession,
                    downloadTask: NSURLSessionDownloadTask,
                    didFinishDownloadingToURL location: NSURL){
        
        let path = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        print(path)
        let documentDirectoryPath:String = path[0]
        let fileManager = NSFileManager()
        let destinationURLForFile = NSURL(fileURLWithPath: documentDirectoryPath.stringByAppendingString("/216_sd_layout_and_animation_techniques_for_watchkit.mp4"))
        
        if fileManager.fileExistsAtPath(destinationURLForFile.path!){
            showFileWithPath(destinationURLForFile.path!)
        }
        else{
            do {
                try fileManager.moveItemAtURL(location, toURL: destinationURLForFile)
                // show file
                showFileWithPath(destinationURLForFile.path!)
            }catch{
                print("An error occurred while moving file to destination url")
            }
        }
    }
    // 2
    func URLSession(session: NSURLSession,
                    downloadTask: NSURLSessionDownloadTask,
                    didWriteData bytesWritten: Int64,
                                 totalBytesWritten: Int64,
                                 totalBytesExpectedToWrite: Int64){
        self.tableViewCell.statusLabel.text = "Download \(Int(totalBytesWritten)/1000000)MB of \(Int(totalBytesExpectedToWrite)/1000000)MB"
        self.tableViewCell.progressBar.setProgress(Float(totalBytesWritten)/Float(totalBytesExpectedToWrite), animated: true)
        let proc = Int(Float(totalBytesWritten)*100/Float(totalBytesExpectedToWrite))
        self.tableViewCell.procent.text = "\(proc)%"
    }
    
    func showFileWithPath(path: String){
        let isFileFound:Bool? = NSFileManager.defaultManager().fileExistsAtPath(path)
        if isFileFound == true{
            let viewer = UIDocumentInteractionController(URL: NSURL(fileURLWithPath: path))
            viewer.delegate = self
            viewer.presentPreviewAnimated(true)
        }
    }
    
    
    func URLSession(session: NSURLSession,
                    task: NSURLSessionTask,
                    didCompleteWithError error: NSError?){
        downloadTask = nil
//        progressView.setProgress(0.0, animated: true)
        if (error != nil) {
            print(error?.description)
        }else{
            print("The task finished transferring data successfully")
        }
    }
    
    
}
